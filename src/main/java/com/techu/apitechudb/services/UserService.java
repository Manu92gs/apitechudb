package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public List<UserModel> findByAge(int age) {
        System.out.println("findByAge en UserService");

        return this.userRepository.findByAge(age);
    }

    public UserModel updateUser(UserModel user) {
        System.out.println("updateUser en UsertService");

        return this.userRepository.updateUser(user);
    }

    public boolean deleteUser(String id) {
        System.out.println("deleteUser en UserService");

        boolean result = false;

        Optional<UserModel> userTodelete = this.findById(id);

        if (userTodelete.isPresent()) {
            result = true;
            this.userRepository.deleteUser(userTodelete.get());
        }

        return result;
    }
}
