package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
        @Autowired
        ProductRepository productRepository;

        public List<ProductModel> findAll() {
            System.out.println("findAll en ProductService");

            return this.productRepository.findAll();
        }

        public ProductModel add(ProductModel product) {
            System.out.println("add en ProductService");

            return this.productRepository.save(product);
        }

        public Optional<ProductModel> findById(String id) {
            System.out.println("findById en ProductService");

            return this.productRepository.findById(id);
        }

        public ProductModel updateProduct(ProductModel product) {
            System.out.println("updateProduct en ProductService");

            return this.productRepository.updateProduct(product);
        }

        public boolean deleteProduct(String id) {
            System.out.println("deleteProduct en ProductService");

            boolean result = false;

            Optional<ProductModel> productTodelete = this.findById(id);

            if (productTodelete.isPresent()) {
                result = true;
                this.productRepository.deleteProduct(productTodelete.get());
            }

            return result;
        }
}
