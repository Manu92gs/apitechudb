package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ProductService productService;

    public List<PurchaseModel> getCompras() {
        System.out.println("getCompras en PurchaseService");

        return this.purchaseRepository.getCompras();
    }

    public Optional<PurchaseModel> getCompra(String id) {
        System.out.println("getCompra en PurchaseService");

        return this.purchaseRepository.getCompra(id);
    }

    public PurchaseModel addCompra(PurchaseModel purchase) {
        System.out.println("addCompra en PurchaseService");

        return this.purchaseRepository.addCompra(purchase);
    }

    public Map<Boolean,Integer> finalizarCompra(PurchaseModel purchase){

        Map<Boolean,Integer> resultadoCompra = new HashMap<>();
        boolean encontrado = true;
        int totalCompra = 0;
        ProductModel productoABuscar = new ProductModel();

        for(Map.Entry<String, Integer> productosEnCompra : purchase.getPurchaseItems().entrySet()) {
            Optional<ProductModel> respuestaProductoExiste = productService.findById(productosEnCompra.getKey());
            int productAmount = productosEnCompra.getValue();
            if(respuestaProductoExiste.isEmpty()) {
                System.out.println("Producto no encontrado: " +productoABuscar.getId());
                encontrado = false;
            }else {
                productoABuscar = respuestaProductoExiste.get();
                System.out.println("total compra actual: "+totalCompra);
                System.out.println("precio producto a comprar: "+productoABuscar.getPrice());
                System.out.println("cantidad producto a comprar: "+ productAmount);
                totalCompra = totalCompra + (productoABuscar.getPrice() * productAmount);
                System.out.println("total compra tras añadir producto: "+totalCompra);
            }
        }

        resultadoCompra.put(encontrado,totalCompra);

        return resultadoCompra;
    }
}
