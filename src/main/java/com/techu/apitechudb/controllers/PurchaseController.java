package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    UserService userService;

    @GetMapping("/compra")
    public ResponseEntity<List<PurchaseModel>> getCompras() {
        System.out.println("getCompras");

        return new ResponseEntity<>(this.purchaseService.getCompras(), HttpStatus.OK);
    }

    @PostMapping("/compra")
    public ResponseEntity<Object> addCompra(@RequestBody PurchaseModel purchase) {
        int totalCompra = 0;

        System.out.println("addCompra");
        System.out.println("La id de la compra a crear es: "+purchase.getId());
        System.out.println("El id del usuario que realiza la compra es: "+purchase.getUserId());
        System.out.println("El total de la compra es: "+purchase.getAmount());
        System.out.println("Los productos comprados son: ");
        purchase.getPurchaseItems().forEach((Object itemID,Object amount) -> {
            System.out.println("El producto comprado es:"+itemID);
            System.out.println("La cantidad del producto comprado es: "+ amount);
        });

        //Definimos respuesta
        ResponseEntity<Object> respuestaCompra;

        //Comprobamos que idUsuario existe
        respuestaCompra = comprobarSiUsuarioExiste(purchase);

        //Comprobamos que idCompra existe
        if(respuestaCompra == null) { respuestaCompra = comprobarIdCompra(purchase);}

        //Comprobamos que existan todos los productos y finalizamos compra.
        if (respuestaCompra == null) {respuestaCompra = finalizarCompra(purchase); }

        return respuestaCompra;
    }

    private ResponseEntity<Object> comprobarSiUsuarioExiste(PurchaseModel purchase) {
        System.out.println("Comprobamos si el usuario existe.");
        Optional<UserModel> respuestaUsuarioExiste = userService.findById(purchase.getUserId());
        if(respuestaUsuarioExiste.isEmpty()) {
            return new ResponseEntity<>("La compra no se pudo realizar, porque el usuario "+purchase.getUserId()+" no existe.",HttpStatus.UNAUTHORIZED);
        }else{
            System.out.println("El usuario "+purchase.getUserId()+" si existe.");
            return null;
        }
    }

    private ResponseEntity<Object> comprobarIdCompra(PurchaseModel purchase) {
        System.out.println("Comprobamos si existe una compra con la id: "+purchase.getId());
        Optional<PurchaseModel> respuestaCompraIdExiste = purchaseService.getCompra(purchase.getId());
        if (respuestaCompraIdExiste.isPresent()) {
            return new ResponseEntity<>("La compra no se pudo realizar, porque ya existe una id con esa compra. (ID compra: "+purchase.getId()+")",HttpStatus.CONFLICT);
        }else{
            System.out.println("No existe compra con la ID: "+purchase.getId());
            return null;
        }
    }


    private ResponseEntity<Object> finalizarCompra(PurchaseModel purchase) {
        Map<Boolean,Integer> comprobarCompra = purchaseService.finalizarCompra(purchase);
        Map.Entry<Boolean,Integer> resultadoCompra = comprobarCompra.entrySet().iterator().next();
        if(resultadoCompra.getKey()) {
            purchase.setAmount(resultadoCompra.getValue());
            return new ResponseEntity<>(this.purchaseService.addCompra(purchase),HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>("La compra no se pudo realizar, porque algun producto de la lista no existe.",HttpStatus.NOT_FOUND);
        }
    }
}
