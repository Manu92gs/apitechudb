package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(@RequestParam(required = false,defaultValue = "0") int age) {

        ResponseEntity<Object> response;

        if(age<=0) {
            System.out.println("getUsers");
            response = new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
        }else {
            System.out.println("getUsersbyAge");
            System.out.println("La edad a buscar es: "+age);

            List<UserModel> result = this.userService.findByAge(age);

            response = new ResponseEntity<>(
                    result.isEmpty() ? "No se han encontrado usuarios con esas edad." : result,
                    result.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK
            );
        }

        return response;
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("El usuario a buscar es: "+id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es: "+user.getId());
        System.out.println("El nombre del usuario a crear es: "+user.getName());
        System.out.println("La edad del usuario a crear es: "+user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar en parametro es "+id);
        System.out.println("La id del usuario a actualizar es: "+user.getId());
        System.out.println("El nombre del usuario es: "+user.getName());
        System.out.println("La edad del usuario es: "+user.getAge());

        return new ResponseEntity<>(this.userService.updateUser(user),HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es: "+id);

        boolean deleteUser = this.userService.deleteUser(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
