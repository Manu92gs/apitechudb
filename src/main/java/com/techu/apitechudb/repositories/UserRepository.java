package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {
    public List<UserModel> findAll() {
        System.out.println("findAll en UserRepository");

        return ApitechudbApplication.users;
    }

    public UserModel save(UserModel user) {
        System.out.println("Save en UserRepository");

        ApitechudbApplication.users.add(user);

        return user;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserRepository");

        Optional<UserModel> result = Optional.empty();

        for(UserModel userInList : ApitechudbApplication.users) {
            if(userInList.getId().equals(id)) {
                System.out.println("Usuario encontrado");
                result = Optional.of(userInList);
            }
        }
        return result;
    }

    public List<UserModel> findByAge(int age) {
        System.out.println("findByAge en UserRepository");

        List<UserModel> usersFounded = new ArrayList<>();

        for(UserModel usersInList : ApitechudbApplication.users) {
            if(usersInList.getAge() == age) {
                usersFounded.add(usersInList);
            }
        }

        return usersFounded;
    }

    public UserModel updateUser(UserModel user) {
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate = this.findById(user.getId());

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario encontrado");

            UserModel userFromList = userToUpdate.get();

            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }
        return user;
    }

    public void deleteUser(UserModel userToDelete) {
        System.out.println("deleteUser en UserRepository");
        System.out.println("Borrando usuario");

        ApitechudbApplication.users.remove(userToDelete);

    }
}
