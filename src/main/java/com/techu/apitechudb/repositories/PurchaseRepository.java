package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> getCompras() {
        System.out.println("getCompras en PurchaseRepository.");
        return  ApitechudbApplication.purchases;
    }

    public PurchaseModel addCompra(PurchaseModel purchase) {
        System.out.println("AddCompra en PurchaseRepository.");

        ApitechudbApplication.purchases.add(purchase);

        return purchase;
    }

    public Optional<PurchaseModel> getCompra(String id) {
        System.out.println("GetCompra en PurchaseRepository");
        System.out.println("Id a buscar: "+id);

        Optional<PurchaseModel> result = Optional.empty();

        for(PurchaseModel purchaseInList : ApitechudbApplication.purchases) {
            if(purchaseInList.getId().equals(id)) {
                result = Optional.of(purchaseInList);
            }
        }

        return result;

    }

}
