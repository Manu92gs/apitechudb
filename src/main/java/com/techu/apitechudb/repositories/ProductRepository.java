package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductRepository");

        return ApitechudbApplication.productModels;
    }

    public ProductModel save(ProductModel product) {
        System.out.println("Save en ProductRepository");

        ApitechudbApplication.productModels.add(product);

        return product;
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductRepository");

        Optional<ProductModel> result = Optional.empty();

        for(ProductModel productInList : ApitechudbApplication.productModels) {
            if(productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = Optional.of(productInList);
            }
        }
        return result;
    }

    public ProductModel updateProduct(ProductModel product) {
        System.out.println("update en ProductRepository");

        Optional<ProductModel> productToUpdate = this.findById(product.getId());

        if (productToUpdate.isPresent()) {
            System.out.println("Producto encontrado");

            ProductModel productFromList = productToUpdate.get();

            productFromList.setDesc(product.getDesc());
            productFromList.setPrice(product.getPrice());
        }
        return product;
    }

    public void deleteProduct(ProductModel productToDelete) {
        System.out.println("deleteProduct en ProductRepository");
        System.out.println("Borrando producto");

        ApitechudbApplication.productModels.remove(productToDelete);

    }
}
