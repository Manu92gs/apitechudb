package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> users;
	public static ArrayList<PurchaseModel> purchases = new ArrayList<>();

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);
		productModels = ApitechudbApplication.getTestData();
		users = ApitechudbApplication.getUsersData();
	}

	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(new ProductModel("1","Producto 1",10));

		productModels.add(new ProductModel("2","Producto 2",20));

		productModels.add(new ProductModel("3","Producto 3",30));

		productModels.add(new ProductModel("4","Producto 4",40));

		return productModels;
	}

	private static ArrayList<UserModel> getUsersData() {

		ArrayList<UserModel> users = new ArrayList<>();

		users.add(new UserModel("1","Manu",20));

		users.add(new UserModel("2","Paca",20));

		users.add(new UserModel("3","Federico",63));

		return users;
	}

}
